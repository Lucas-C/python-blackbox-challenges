<https://lucas-c.frama.io/python-blackbox-challenges/>

**Rules** :

- do NOT open the ZIP
- do NOT explore the call stack (_e.g._ with a debugger)
- beat the challenge !

Need `--help` ? Be `--explicit`.

# Simple runner usage

    $subdir/runner.py

Randomness can be controlled by defining a `$SEED`.

# Building black box zip apps

    ./zipapp.sh $subdir $outfilename

# Website iterative development

    pip install livereload
    public/watch_and_serve.py

# Images sources

- [Enigma Machine photo](https://www.flickr.com/photos/ciagov/5416145175/in/photostream/)
- `cards/classic/python-card.png` from [u/denholmsdead](https://www.reddit.com/user/denholmsdead) "Linux Pictures", totally free but [not available on linux.pictures nor the original GitHub repo anymore](https://www.reddit.com/r/linux/comments/bvp1tq/linux_pictures_no_longer_available/)
- [boîte-package-bois-scellé from pixabay.com](https://pixabay.com/fr/vectors/bo%C3%AEte-package-bois-scell%C3%A9-159986/)
- [Grain Drawing Venomous Snake Snake Water Snake from maxpixel.net](https://www.maxpixel.net/Grain-Drawing-Venomous-Snake-Snake-Water-Snake-332610) - CC0


<!--Ideas / next steps / WIP:
- add .pyz download counters? e.g. using nginx like for blog-pdf-downloads.log
- autres idées de puzzle:
  * minesweeper
  * Simon says
- mentionner: hard + programming puzzle
- pb de certs: https://python-blackbox-challenges.chezsoi.org/
  * nécessite d'étendre celui de chezsoi.org en y incluant python-blackbox-challenges.chezsoi.org
  * je viens d'essayer (2019/08/07) et je me suis heurté à une erreur de letsencrypt comme quoi il n'arrive pas à résoudre ce DNS
- make boxes black
- public/ : leaderboard with form for claiming flags
  * single entry per IP
  * validate the flag provided before inserting
  * tables content exposed through GET requests
- reveal flags when each challenge si completed
- Snake: compute score : faster is better
- VerticalRaceGame: build a rand level generator ?

Promouvoir :
- via collègues & potos : Mathieu Lucas, Antoine B, Francis
- via blog
- via afpy
- sur r/python
- https://pycoders.com/submissions
- https://linuxfr.org/liens
- https://news.humancoders.com/url/new
- https://news.ycombinator.com
-->
