#!/bin/bash
set -o errexit -o nounset
SUBDIR=$1
shift
pyz=$PWD/$SUBDIR.pyz
cd $SUBDIR
mv answer.py ../answer.py.bak
python -m zipapp --output $pyz --main runner:main_with_defaults $PWD
mv ../answer.py.bak answer.py
echo Built:
ls -lh $pyz
echo
echo To try:
echo python $pyz
echo
echo Now performing insta-test:
# We change directory to really import modules in the ZIP and not directly files in here:
cd ..
python $pyz "$@"
if python $pyz "$@"; then exit 1; fi
python $pyz --answercode-filepath $SUBDIR/answer.py "$@"
rm answer.py
