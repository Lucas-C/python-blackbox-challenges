#!/usr/bin/env python3

from os.path import dirname
from livereload import Server

SCRIPT_DIR = dirname(__file__)

SERVER = Server()
SERVER.watch(SCRIPT_DIR + '/index.html')
SERVER.watch(SCRIPT_DIR + '/assets/css/main.css')
SERVER.watch(SCRIPT_DIR + '/assets/js/main.js')
SERVER.serve(root=SCRIPT_DIR)
